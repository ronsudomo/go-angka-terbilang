package terbilang

import (
	"fmt"
	"log"
	"strings"
)

// Angka dasar
var baseNum = []string{
	"nol",
	"satu",
	"dua",
	"tiga",
	"empat",
	"lima",
	"enam",
	"tujuh",
	"delapan",
	"sembilan",
	"sepuluh",
	"sebelas",
}

// Group ribuan
var thousandGroupings = []string{
	"",
	"ribu",
	"juta",
	"milyar",
	"triliun",
	"kuadriliun",
}

// AngkaTerbilang memberikan jumlah terbilang (dalam Bahasa Indonesia) dari angka num yang diberikan.
func AngkaTerbilang(num int) string {
	if num < 0 {
		return "minus " + AngkaTerbilang(-num)
	}

	if num == 0 {
		return baseNum[0]
	}

	chunks := SplitThousands(num)
	chunkCount := len(chunks)
	numWords := []string{}

	// Check jika melebihi angka maksimal yg dapat diterima
	if chunkCount > len(thousandGroupings) {
		return fmt.Sprintf("seribu %s atau lebih", thousandGroupings[len(thousandGroupings)-1])
	}

	for i, n := range chunks {
		if n > 0 {
			grouping := thousandGroupings[chunkCount-i-1]
			if grouping == "ribu" && n == 1 {
				numWords = append(numWords, "seribu")
			} else {
				numWords = append(numWords, terbilangRatusan(n)+" "+grouping)
			}
		}
	}

	return strings.TrimSpace(strings.Join(numWords, " "))
}

// terbilangRatusan Memberikan jumlah terbilang untuk angka > 0 dan < 1000.
func terbilangRatusan(num int) string {
	if num < 1 || num > 999 {
		log.Fatal("Angka harus antara 1 dan 999")
	}

	numWords := ""

	if num < 12 {
		numWords = baseNum[num]
	} else if num < 20 {
		numWords = baseNum[num-10] + " belas"
	} else if num < 100 {
		numWords = baseNum[num/10] + " puluh "
		remainder := num % 10
		if remainder > 0 {
			numWords += baseNum[remainder]
		}
	} else {
		angkaRatusan := num / 100
		if angkaRatusan == 1 {
			numWords = "seratus "
		} else {
			numWords = baseNum[angkaRatusan] + " ratus "
		}
		remainder := num % 100
		if remainder > 0 {
			numWords += terbilangRatusan(remainder)
		}
	}

	return strings.TrimSpace(numWords)
}

// SplitThousands memecahkan angka yang diberikan ke dalam group ribuan.
func SplitThousands(num int) []int {
	if num == 0 {
		return []int{0}
	}

	chunks := []int{}

	for num != 0 {
		chunks = append([]int{num % 1000}, chunks...)
		num = num / 1000
	}

	return chunks
}
