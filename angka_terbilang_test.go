package terbilang

import (
	"math"
	"testing"

	"gotest.tools/v3/assert"
)

func TestSplitThousands(t *testing.T) {
	testCases := []struct {
		num      int
		expected []int
	}{
		{12345, []int{12, 345}},               // Positive number with single chunk
		{987654321, []int{987, 654, 321}},     // Positive number with multiple chunks
		{-123456789, []int{-123, -456, -789}}, // Negative number
		{0, []int{0}},                         // Zero
	}

	for _, tc := range testCases {
		assert.DeepEqual(t, SplitThousands(tc.num), tc.expected)
	}
}

func TestAngkaTerbilang(t *testing.T) {
	testGroups := [][]testCase{
		testCase_forNegativeNumbers(),
		testCase_forNumbersLargerThanSupported(),
		testCase_forNumbersLessThan100(),
		testCase_forNumbersWithTrailingZeroes(),
		testCase_forNumbersWithTrailingOnes(),
		testCase_forNumbersWithTrailingNines(),
		testCase_forNumbersEndingZeroPlusOne(),
	}

	for _, tg := range testGroups {
		for _, tc := range tg {
			assert.Equal(t, AngkaTerbilang(tc.num), tc.expected)
		}
	}
}

type testCase struct {
	num      int
	expected string
}

func testCase_forNegativeNumbers() []testCase {
	return []testCase{
		{-1, "minus satu"},
		{-1234567890,
			"minus satu milyar " +
				"dua ratus tiga puluh empat juta " +
				"lima ratus enam puluh tujuh ribu " +
				"delapan ratus sembilan puluh"},
	}
}

func testCase_forNumbersLargerThanSupported() []testCase {
	return []testCase{
		{1111111111111111111, "seribu kuadriliun atau lebih"},
		{math.MaxInt64, "seribu kuadriliun atau lebih"},
	}
}

func testCase_forNumbersLessThan100() []testCase {
	return []testCase{
		{0, "nol"},
		{1, "satu"},
		{2, "dua"},
		{3, "tiga"},
		{4, "empat"},
		{5, "lima"},
		{6, "enam"},
		{7, "tujuh"},
		{8, "delapan"},
		{9, "sembilan"},
		{10, "sepuluh"},
		{11, "sebelas"},
		{12, "dua belas"},
		{15, "lima belas"},
		{19, "sembilan belas"},
		{20, "dua puluh"},
		{25, "dua puluh lima"},
		{50, "lima puluh"},
		{55, "lima puluh lima"},
		{90, "sembilan puluh"},
		{95, "sembilan puluh lima"},
	}
}

func testCase_forNumbersWithTrailingZeroes() []testCase {
	return []testCase{
		{100, "seratus"},
		{1000, "seribu"},
		{10000, "sepuluh ribu"},
		{100000, "seratus ribu"},
		{1000000, "satu juta"},
		{10000000, "sepuluh juta"},
		{100000000, "seratus juta"},
		{1000000000, "satu milyar"},
		{10000000000, "sepuluh milyar"},
		{100000000000, "seratus milyar"},
		{1000000000000, "satu triliun"},
		{10000000000000, "sepuluh triliun"},
		{100000000000000, "seratus triliun"},
		{1000000000000000, "satu kuadriliun"},
	}
}

func testCase_forNumbersWithTrailingOnes() []testCase {
	return []testCase{
		{111, "seratus sebelas"},
		{1111, "seribu seratus sebelas"},
		{11111, "sebelas ribu seratus sebelas"},
		{111111, "seratus sebelas ribu seratus sebelas"},
		{1111111, "satu juta seratus sebelas ribu seratus sebelas"},
		{11111111, "sebelas juta seratus sebelas ribu seratus sebelas"},
		{111111111, "seratus sebelas juta seratus sebelas ribu seratus sebelas"},
		{1111111111,
			"satu milyar " +
				"seratus sebelas juta seratus sebelas ribu seratus sebelas"},
		{11111111111,
			"sebelas milyar " +
				"seratus sebelas juta seratus sebelas ribu seratus sebelas"},
		{111111111111,
			"seratus sebelas milyar " +
				"seratus sebelas juta seratus sebelas ribu seratus sebelas"},
		{1111111111111,
			"satu triliun " +
				"seratus sebelas milyar " + "seratus sebelas juta " +
				"seratus sebelas ribu " + "seratus sebelas"},
		{11111111111111,
			"sebelas triliun " +
				"seratus sebelas milyar " + "seratus sebelas juta " +
				"seratus sebelas ribu " + "seratus sebelas"},
		{111111111111111,
			"seratus sebelas triliun " +
				"seratus sebelas milyar " + "seratus sebelas juta " +
				"seratus sebelas ribu " + "seratus sebelas"},
		{1111111111111111,
			"satu kuadriliun " + "seratus sebelas triliun " +
				"seratus sebelas milyar " + "seratus sebelas juta " +
				"seratus sebelas ribu " + "seratus sebelas"},
		{11111111111111111,
			"sebelas kuadriliun " + "seratus sebelas triliun " +
				"seratus sebelas milyar " + "seratus sebelas juta " +
				"seratus sebelas ribu " + "seratus sebelas"},
		{111111111111111111,
			"seratus sebelas kuadriliun " + "seratus sebelas triliun " +
				"seratus sebelas milyar " + "seratus sebelas juta " +
				"seratus sebelas ribu " + "seratus sebelas"},
	}
}

func testCase_forNumbersWithTrailingNines() []testCase {
	return []testCase{
		{9, "sembilan"},
		{99, "sembilan puluh sembilan"},
		{999, "sembilan ratus sembilan puluh sembilan"},
		{9999, "sembilan ribu sembilan ratus sembilan puluh sembilan"},
		{99999, "sembilan puluh sembilan ribu sembilan ratus sembilan puluh sembilan"},
		{999999,
			"sembilan ratus sembilan puluh sembilan ribu sembilan ratus sembilan puluh sembilan"},
		{9999999,
			"sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{99999999,
			"sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{999999999,
			"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{9999999999,
			"sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{99999999999,
			"sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{999999999999,
			"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{9999999999999,
			"sembilan triliun " +
				"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{99999999999999,
			"sembilan puluh sembilan triliun " +
				"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{999999999999999,
			"sembilan ratus sembilan puluh sembilan triliun " +
				"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{9999999999999999,
			"sembilan kuadriliun " +
				"sembilan ratus sembilan puluh sembilan triliun " +
				"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{99999999999999999,
			"sembilan puluh sembilan kuadriliun " +
				"sembilan ratus sembilan puluh sembilan triliun " +
				"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
		{999999999999999999,
			"sembilan ratus sembilan puluh sembilan kuadriliun " +
				"sembilan ratus sembilan puluh sembilan triliun " +
				"sembilan ratus sembilan puluh sembilan milyar " +
				"sembilan ratus sembilan puluh sembilan juta " +
				"sembilan ratus sembilan puluh sembilan ribu " +
				"sembilan ratus sembilan puluh sembilan"},
	}
}

func testCase_forNumbersEndingZeroPlusOne() []testCase {
	return []testCase{
		{100 + 1, "seratus satu"},
		{1000 + 1, "seribu satu"},
		{10000 + 1, "sepuluh ribu satu"},
		{100000 + 1, "seratus ribu satu"},
		{1000000 + 1, "satu juta satu"},
		{10000000 + 1, "sepuluh juta satu"},
		{100000000 + 1, "seratus juta satu"},
		{1000000000 + 1, "satu milyar satu"},
		{10000000000 + 1, "sepuluh milyar satu"},
		{100000000000 + 1, "seratus milyar satu"},
		{1000000000000 + 1, "satu triliun satu"},
		{10000000000000 + 1, "sepuluh triliun satu"},
		{100000000000000 + 1, "seratus triliun satu"},
		{1000000000000000 + 1, "satu kuadriliun satu"},
	}
}
